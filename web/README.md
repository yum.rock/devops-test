# Sample web application

- Install dependencies

```
$ docker-compose run --rm node yarn install
```

- Lint check

```
$ docker-compose run --rm node yarn run htmllint-lint
```

- Run in docker without K8s

```
$ docker-compose run --rm node node app
```